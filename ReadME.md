## 3D Printer Assistant

## Description
3D Printing has allowed a new generation of manufacturing to occur in the home and workplace. This has enabled rapid prototyping and small scale production runs. However, such production runs still require large amounts of mannual handling; prominently removing the print from the build plate after it has completed. Using multiple printers can increase production capabilities but it also requires someone to be around to remove the prints and enable the printer to keep printing. 
The 3D Printer Assistant's main goal is to remove the human aspect and enable printers to print continuously by detecting the print is complete and removing the printed object(s) to enable another print to begin. For this iteration, small desktop sized i3 style printers such as the Ender 3 or Prusa i3Mk3s+ will be used as concepts.

## Visuals
Insert some screenshots of simulation here.

## Installation
Insert Installation guide here

## Usage
List different printers that may be elligible for use here

## Support
Please contact Alex for further support:
    alexander.w.robinson@student.uts.edu.au

## Roadmap
List some random future ideas for the system here

## Project status
Active




## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.
