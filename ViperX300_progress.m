clf
view(3);

% joints 4 and 7 are extra
name = ['VIPREX300',datestr(now,'yyyymmddTHHMMSSFFF')];
L1 = Link('d',0.12675,'a',0,'alpha',pi/2,'qlim',deg2rad([-180 180]), 'offset',0); % correct
L2 = Link('d',0,'a',0.30594,'alpha',0,'qlim', deg2rad([-101 101]), 'offset',pi/2-deg2rad(11.3)); % using diagonal for now, change this into proper shape later
L3 = Link('d',0,'a',0.3,'alpha',0,'qlim', deg2rad([-101 92]), 'offset', -pi/2+deg2rad(11.3)); % correct
L4 = Link('d',0,'a',0,'alpha',pi/2,'qlim',deg2rad([0 0]),'offset', pi/2); %% needed to get right angle
L5 = Link('d',0.10362,'a',0,'alpha',pi/2,'qlim',deg2rad([-107 130]),'offset',0); 
L6 = Link('d',0,'a',0.07,'alpha',0,'qlim',deg2rad([-180 180]), 'offset',pi/2);
L7 = Link('d',0,'a',0,'alpha',pi/2,'qlim',deg2rad([0 0]), 'offset',pi/2);%% needed to get length on right direction
L8 = Link('d',0.0687,'a',0,'alpha',0,'qlim',deg2rad([-180 180]), 'offset', 0);    


ROBOT = SerialLink([L1 L2 L3 L4 L5 L6 L7 L8]);
ROBOT.plot([zeros(1,8)],'noarrow');
axis equal;

%%
for i = -pi:pi/20:pi
    ROBOT.plot([i,0,0,0,0,0,0,0],'noarrow');
end
for i = -100*pi/180:pi/20:100*pi/180
    ROBOT.plot([0,i,0,0,0,0,0,0],'noarrow');
end
for i = -100*pi/180:pi/20:90*pi/180
    ROBOT.plot([0,0,i,0,0,0,0,0],'noarrow');
end
for i = -100*pi/180:pi/20:100*pi/180
    ROBOT.plot([0,0,0,0,i,0,0,0],'noarrow');
end
for i = -pi/2:pi/20:pi/2
    ROBOT.plot([0,0,0,0,0,i,0,0],'noarrow');
end
for i = -pi/2:pi/20:pi/2
    ROBOT.plot([0,0,0,0,0,0,0,i],'noarrow');
end


%{ 
choosing an arm
TWO OPTIONS:

OPTION (1)
in toolbox: DOBOT magician on a linear rail FOR PICKING PRINTS OFF BED AND
PLACING ON PALLETS
- compact and lightweight
- range of grippers and mods
- rail is perfect to extend to reach to 2 or 3 printers
- max payload of 500g is sufficient for pla printing
- max reach of 320 mm is sufficent if lined up along printers

NOT in toolbox: ViperX 300 Robot Arm (Interbotix) FOR PICKING UP PRINTS OFF
PALETS AND PACKING INTO BOXES
- Large reach of 750mm
- repeatability of 1mm which is sufficent
- working payload of 750g which should be sufficient for moving singular prints
- thin arms allows it to be compact when packed away -> good for workshop
- cheaper option
- https://www.trossenrobotics.com/viperx-300-robot-arm-6dof.aspx


OPTION (2)
NOT in toolbox: Niryo One FOR PICKING PRINTS OFF BED AND
PLACING ON PALLETS
- open source tech with parts can be 3D printed -> cheaper!
- as a result may have less reliability/precision
- documented in detail
- compact and lightweight
- no set linear rail mod -> will be harder to integrate
- https://niryo.com/docs/niryo-one/user-manual/mechanical-specifications/

in toolbox: UR3 FOR PICKING UP PRINTS OFF
PALETS AND PACKING INTO BOXES
- well-developed, industry proven -> great reliabilty/precision
- familiar with and easy to use 
- 500mm reach which should be sufficent for packing boxes
- sufficient 3kg payload limit

%}

