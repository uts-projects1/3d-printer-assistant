%% Assignment 2 - 3D Printer Assistant
clf;
clc;
hold on;


%Crucial Parameters
startTrans = [0,0,0.5];             %Starting transformation given
steps = 25;                         %Steps in each animation loop
positionLog = cell(9,6);               %Initialising a cell array to log all key motion transforms


%Scene Size (Halved)
workSpace = 1.5;                    %Defining the size of the workspace, centre to edge

%Create the Scene
makeScene(workSpace, startTrans);   %Import all the ply files needed to create the scene

%Set Axis
axis([startTrans(1)-workSpace,startTrans(1)+workSpace,...         %Set the axis of the scene based on the workSpcace and startTrans
    startTrans(2)-workSpace,startTrans(2)+workSpace,...
    startTrans(3)-0.5,startTrans(3)+1]);

%Create Dobot
Dobot = LinearDobot(startTrans + [-0.5,0.35,0]);
initialq  = [1,-pi/2,20*pi/180,85*pi/180,75*pi/180];
moveRobot(Dobot, [0.83,0.35,0.73], 50, initialq, 0);

%Set View
view([1.5, 1.5, 2]);


%% Teach
Dobot.model.teach();

%% Operation
clc

%Array of joint angle guesses that are used multiple times throughout the
%operation process

guessArray = [-1, 0, 50*pi/180, 85*pi/180, 45*pi/180; % Primed Right
              -1, 0, 5*pi/180, 85*pi/180, 90*pi/180; % Reached Right    
            ];

%2D Array that the 1st Dimension contains all the different XYZ coordinates that the
%end-effector needs to move to throughout the operation process. Using this
%2D Array enables the single moveRobot function to be used within a for
%loop 

positionArray = [0.5, 0.569, 0.625, 0, 0;
                 0.5, 0.681, 0.774, 0, 0;
                 0.5, 0.615, 0.625, 0, 0;
                 ];

%2D Array that the 2nd Dimension contains all the different guesses that
%are required at each step of the operation process. These guesses are
%taken from the guess array and used mulitple times. 

positionArray(:,:,2) = [guessArray(1,:);
                        guessArray(2,:); 
                        guessArray(1,:); 
                        ];

%% Operation Process
%This For Loop moves through all the positions and guesses enabling the
%robot to move

for i = 1:size(positionArray)
    moveRobot(Dobot, positionArray(i,[1 2 3],1), 50, positionArray(i,:,2), i);
end


%% Move Robot
%moveRobot takes in the robot, the next transform the robot needs to move
%towards, the amount of steps to be taken between current position and the
%next, the guess for the join angles and what position number the operation
%is up to. 
%This enables move functions to be simplified and repeated in a for loop.

function moveRobot(robot, nextTrans, steps, guess, pos)
currentq = robot.model.getpos();
nextq = robot.model.ikcon(transl(nextTrans), guess);
motion = jtraj(currentq, nextq, steps);
        
for i = 1:steps
    robot.model.animate(motion(i,:));
    drawnow();
end

logPosition(robot, pos);
end


%% Log Position
%As the position of the robot moves throughout the operation process, the
%location of the endeffector will get logged in the command window and
%stored to an array which can be accessed and assessed at a later point.

function logPosition(robot, pos)
 temp = round(robot.model.fkine(robot.model.getpos),2);                              %Get the current transform of the end effector, rounded to 2dp
 fprintf('Position %d Coords: \nX: %g Y: %g Z: %g\n',...                          %Display XYZ components of transform in command window
 pos, temp(1,4), temp(2,4), temp(2,4));           
end


%% Create Scene
%Uses the makeObject function repeatedly for different objects to place
%them in the scene. 
%Locations of each object are calculated using the startTrans transform
%given
%Sets camlight once for the whole scene
function makeScene(workSpace, startPos)

%Place Concrete Floor
surf([startPos(1)-workSpace,startPos(1)-workSpace;workSpace+startPos(1),workSpace+startPos(1)] ...
    ,[startPos(2)+workSpace,startPos(2)-workSpace;startPos(2)+workSpace,startPos(2)-workSpace] ...
    ,[startPos(3)-0.49,startPos(3)-0.49;startPos(3)-0.49,startPos(3)-0.49],'CData',imread('concrete.jpg'),'FaceColor','texturemap');

%Place Table
MakeObject('table',(startPos - [0,0,0.5]));

%Place 3D Printers
MakeObject('prusa',(startPos - [0,0,0.5] + [-0.5, 0.7, 0.5]));
MakeObject('prusa',(startPos - [0,0,0.5] + [   0, 0.7, 0.5]));
MakeObject('prusa',(startPos - [0,0,0.5] + [ 0.5, 0.7, 0.5]));

%Place 3D Prints

%Place Boxes for depositing

%Place Office Wall
MakeObject('wall',(startPos - [0,0,0.5]));

%Place Robot E-Stop
MakeObject('eStop',(startPos - [0,0,0.5] + [0.9,-0.9,0.5]));

%Place Robot Barrier
MakeObject('barrier2',(startPos - [0,0,0.5] + [0,1.1,0]));

%Place Fire Extinguisher
MakeObject('extinguisher',(startPos - [0,0,0.5] + [1.3,-1.39,0.3]));

%Place Monitoring Camera
MakeObject('cam',(startPos - [0,0,0.5] + [-1.5,-1.4, 1.4]));

%Place Warning Tape
MakeObject('tape',(startPos - [0,0,0.5] + [1.35,0,0.02]));

camlight;
end


%% Make Object
%Simplifies PlaceObject
function MakeObject(object, coords)
PlaceObject(append(object,'.ply'), coords);
end